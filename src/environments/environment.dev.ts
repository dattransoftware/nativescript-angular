export const environment = {
    production: false,
    apiUrl: 'http://localhost:4000/api',
    NODE_ENV: 'develop',
    LANGUAGE: {
        DEFAULT: 'en',
        DIRECTORY: '/app/assets/i18n/',
        EXTENSION: '.json'
    }
};