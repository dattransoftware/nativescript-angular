export const environment = {
    production: true,
    apiUrl: 'http://foo.bar/api',
    NODE_ENV: 'production',
    LANGUAGE: {
        DEFAULT: 'en',
        DIRECTORY: '/app/assets/i18n/',
        EXTENSION: '.json'
    }
};