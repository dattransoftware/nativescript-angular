// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { enableProdMode } from '@angular/core';
import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./app/app.module";
import { environment } from './environments/environment';

if (environment.production) {
    enableProdMode();
}

platformNativeScriptDynamic({ createFrameOnBootstrap: true }).bootstrapModule(AppModule);
