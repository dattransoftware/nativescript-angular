import { Action, createReducer, on, State } from '@ngrx/store';
import { authActions } from './actions';
import { Auth, initialState } from './state';

const authProducer = createReducer(
    initialState,
    on(authActions.loginSuccess, (state, action) => ({
        ...state,
        _id: action.payload.user._id,
        email: action.payload.user.username,
        token: action.payload.user.token
    })),
    on(authActions.loginFailure, (state, action) => ({
        ...state,
        error: action.payload.error
    }))
);

export function reducer(state: Auth | undefined, action: Action) {
    return authProducer(state, action);
}