import { createAction, props } from '@ngrx/store';
import CONST, { Credentials } from './const';

const login = createAction(
    CONST._ACTION.LOGIN,
    props<{ credentials: Credentials }>()
);

const loginSuccess = createAction(
    CONST._ACTION.LOGIN_SUCCESS,
    props<{ payload: { user } }>()
);

const loginFailure = createAction(
    CONST._ACTION.LOGIN_FAILURE,
    props<{ payload: { error } }>()
);

export const authActions = {
    login,
    loginSuccess,
    loginFailure
};