const _ACTION = {
    LOGIN: '[Login Page] Login',
    LOGIN_SUCCESS: '[Login Page] Success',
    LOGIN_FAILURE: '[Login Page] Failure'
};

export class Credentials {
    email: string;
    password: string;
}

export default {
    _ACTION
}