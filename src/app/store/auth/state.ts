export interface Auth {
    _id: string;
    email: string;
    token: string;
    role: string;
}

export const initialState: Auth = {
    _id: '',
    email: '',
    token: '',
    role: '',
};
