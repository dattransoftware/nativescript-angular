import { NgModule } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
    ]
})
export class CoreStoreModule { }