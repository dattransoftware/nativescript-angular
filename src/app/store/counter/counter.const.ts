export const FEATURE_NAME = 'counter';

export const _ACTION = {
    GET_ALL: '[Counters Page] Get All',
    GET_ALL_SUCCESS: '[Counters API] Counters Loaded Success',
    GET_ALL_FAILED: '[COunters API] COunters Loaded Failed'
};