import { TranslateService } from '@ngx-translate/core';
import { environment } from '~/environments/environment';
import * as Platform from 'tns-core-modules/platform';

/**
 * Init Localization
 * @param {import('ngx-translate').TranslateService} translate
 */
export function initLocalize(translate: TranslateService) {
    translate.setDefaultLang(environment.LANGUAGE.DEFAULT);
    translate.use(Platform.device.language);
}

/**
 * Change other language
 * @param {String} lang
 */
export function changeLanguage(lang: String) {
    this.translate.use(lang);
}

export function get(key: String): String {
    return (this.translate.get(key) as any).value;
}

