import { Component, ChangeDetectorRef } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import * as localize from './assets/i18n/index';

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
    styleUrls: [ "app.component.css" ]
})
export class AppComponent {
    /**
     * @param changeDetectorRef
     */
    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private translate: TranslateService
    ) {
        /**
         * Fix The angular localization pipe does not work
         */
        setTimeout(() => this.changeDetectorRef.detectChanges(), 0);
        /**
         * Initialize localize
         * Set default lang en
         * Use translate platform device lang
         */
        localize.initLocalize(this.translate);
    }
}
