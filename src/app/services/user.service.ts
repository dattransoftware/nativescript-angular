import { Injectable } from "@angular/core";
import * as Kinvey from "kinvey-nativescript-sdk";
import { User } from "../models/user.model";
import { axios } from '~/app/common/helpers/api/index';
import { BASE_API_URL, API_URL } from '~/app/common/configs/api';

@Injectable()
export class UserService {
    register(user: User) {
        return new Promise((resolve, reject) => {
            Kinvey.User.logout()
                .then(() => {
                    Kinvey.User.signup({ username: user.email, password: user.password })
                        .then(resolve)
                        .catch((error) => { this.handleErrors(error); reject(); })
                })
                .catch((error) => { this.handleErrors(error); reject(); });
        });
    }

    login(user: User) {
        return new Promise((resolve, reject) => {
            Kinvey.User.logout()
                .then(() => {
                    Kinvey.User.login(user.email, user.password)
                        .then(resolve)
                        .catch((error) => { this.handleErrors(error); reject(); })
                })
                .catch((error) => { this.handleErrors(error); reject(); });
        });
    }

    resetPassword(email) {
        return Kinvey.User.resetPassword(email)
            .catch(this.handleErrors);
    }

    handleErrors(error: Kinvey.Errors.BaseError) {
        console.error(error.message);
    }

    getDataFromApi(user: User) {
        return new Promise((resolve, reject) => {
            axios.post( BASE_API_URL + API_URL.LOGIN, {username: user.email, password: user.password })
                 .then(resolve)
                 .catch((error) => { this.handleErrors(error); reject(); });
        });
    }
}
