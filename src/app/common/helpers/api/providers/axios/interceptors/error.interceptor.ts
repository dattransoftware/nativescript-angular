import { RESPONSE_CODE } from '~/app/common/constants/response.code';
import Storage from '~/app/common/helpers/storage/local.storage';
import { environment } from '~/environments/environment';

export const ErrorIntercept = (response) => {
	response.use(
		data => {
			return data;
		},
		err => {
			if (err.response && err.response.status === RESPONSE_CODE.UNAUTHORIZED) {
				// Handle error
				console.log(err);
			}

			return Promise.reject(err);
		},
	);
};

export function handleApiError(error) {
	const res = error.response || error.message || error;

	if (environment.NODE_ENV !== 'production') {
		console.error('API Error', res);
	}

	if (res === 'Network Error') {
		let response = { error: `There were some errors with your request: ${res}` };
		throw response;
	}

	if (res.status === RESPONSE_CODE.UNPROCESSABLE_ENTITY && res.data) {
		const errors = [];

		Object.keys(res.data.errors).forEach((field) => {
			res.data.errors[field].forEach((err) => {
				errors.push(err);
			});
		});

		res.error = `There were some errors with your request: ${errors.join(', ')}`;
	}

	if (res.status === RESPONSE_CODE.UNAUTHORIZED) {
		Storage.set('token', '');
		Storage.set('user', '');
		res.error = 'Your session has timed out. Please log in again.';
	}

	if (res.status === RESPONSE_CODE.BAD_REQUEST) {
		res.error = res.data.error;
	}

	if (!res.error) {
		res.error = (res.data && (res.data.message || res.data.errors)) || res.statusText || String(res);
	}

	throw res;
}
