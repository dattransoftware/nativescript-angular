import { authHeader } from '../../../../auth.header';

export const JwtIntercept = (request) => {
	request.use((config) => {
		const method = config.method.toUpperCase();
		const newConfig = {
			...config,
			headers: authHeader()
		};

		if (method === 'PUT' || method === 'DELETE' || method === 'PATCH') {
			newConfig.headers['X-HTTP-Method-Override'] = method;
			newConfig.method = 'post';
		}

		return newConfig;
	});
};
