import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

// Import Modules
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app-routing.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { HttpClientModule, HttpClient, HttpBackend } from '@angular/common/http';
import { ReactiveFormsModule } from "@angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { TranslateCompiler, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CoreStoreModule } from './store/store.module';
import { KinveyModule, UserService } from "kinvey-nativescript-sdk/lib/angular";

// Import Components
import { AppComponent } from "./app.component";
import { LoginComponent } from './modules/login/login.component';

// Import Services
import * as Services from './services/index';

// Import Environment
import { environment } from '../environments/environment';
import { CommonModule } from "@angular/common";

import { appConfig } from './app.config';
import { AuthGuard } from "./auth-guard.service";

// AoT requires an exported function for factories
// Setup directory for localize ./assets/i18n/{lang}.json
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
    return new TranslateHttpLoader(
        http,
        `${environment.LANGUAGE.DIRECTORY}`,
        `${environment.LANGUAGE.EXTENSION}`
    );
}

// Declare Components
const _COMPONENTS = [
    AppComponent,
    LoginComponent
];

// Declare Modules
const _MODULES = [
    NativeScriptModule,
    CommonModule,
    AppRoutingModule,
    NativeScriptFormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreStoreModule,
    NativeScriptHttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (HttpLoaderFactory),
            deps: [HttpClient]
        },
        isolate: true
    }),
    StoreDevtoolsModule.instrument({
        maxAge: 25, // Retains last 25 states
        logOnly: environment.production // Restrict extension to log-only mode
    }),
    KinveyModule.init(appConfig)
];

// Declare Services
const _SERVICES = [
    Services.UserService,
    Services.BackendService,
    Services.UtilityService,
    [AuthGuard]
];

@NgModule({
    bootstrap: [ ..._COMPONENTS ],
    imports: [ ..._MODULES ],
    declarations: [ ..._COMPONENTS ],
    providers: [ ..._SERVICES ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        LoginComponent,
        TranslateModule
    ]
})
export class AppModule { }
